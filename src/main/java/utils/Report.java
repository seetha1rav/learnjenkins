package utils;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public abstract class Report {
	public static ExtentTest logger ;
	public static String testCaseName, testDesc, category , author;
	public static ExtentHtmlReporter html ;
	public static ExtentReports extent;
//@BeforeSuite
	public void startResult() { //these are required for getting report. Hence declared in @Beforesuite  -- as cannot be done for before method
		html = new ExtentHtmlReporter("./reports/result.html");//instead of giving as ExtentHtmlReporter html=new ExtentHtmlReporter("./reports/result.html");
		html.setAppendExisting(false);//instead of giving as html.setAppendExisting(true);//to have history of execution// if given as true,it will keep on appending the history
		extent = new ExtentReports();//instead of giving as ExtentReports extent=new ExtentReports();
		extent.attachReporter(html);//instead of giving as extent.attachReporter(html);
	}
	
	
	public void reportStep(String status, String desc) {
		if (status.equalsIgnoreCase("pass")) {
			logger.log(Status.PASS, desc);			
		} else if (status.equalsIgnoreCase("fail")) {
			logger.log(Status.FAIL, desc);			
		}
	}
	//@AfterSuite
	public void endResult() {
		extent.flush();
	}
	
	
	//@BeforeMethod
	public void beforeMethod() {  // as it required before executing every test 
		logger = extent.createTest(testCaseName, testDesc);//instead of giving as ExtentTest logger = extent.createTest("TC002CreateLead","To create a new Lead");
		logger.assignAuthor(author);//instead of giving as logger.assignAuthor("Seetha");
		logger.assignCategory(category);	//instead of giving as logger.assignCategory("smoke");	
	}
	
	
	
	
	
	
	
	
	
}