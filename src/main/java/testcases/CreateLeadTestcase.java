package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class CreateLeadTestcase extends ProjectMethods{

	@BeforeTest
	public void setData()
	{
		testCaseName="Create Lead";
		testDesc="Create a lead";
		author="Seetha";
		category="smoke";
		dataSheetName="TC001";

	}	



//@DataProvider(name="fetchData", indices= {1})// note:indices cannot be passed here as it is explained in ProjectMethods
	@Test(dataProvider="fetchData")
	public void createLead(String UName1,String UPwd1, String FName1,String LName1, String CName1) {
		new LoginPage() 
		.Typeusername(UName1) //Note: Semicolon is not kept at the end of line as process to be followed in same page
		.TypePassword(UPwd1)
		.ClickLogin()
		.clickCRMSFA()
		.clickCreateLead()
		.TypeFirstName(FName1)
		.TypeLastName(LName1)
		.TypeCompanyName(CName1)
		.clickCreateLeadbtn();
		
	}}