package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class MergeLeadTestCase extends ProjectMethods{
	
	@BeforeTest
	public void setData()
	{
		testCaseName="Merge Lead";
		testDesc="Merge a lead";
		author="Seetha";
		category="smoke";
		dataSheetName="MergeLead";//name of the excel file in the path

	}	

	//@DataProvider(name="fetchData", indices= {1})// note:indices cannot be passed here as it is explained in ProjectMethods
	@Test(dataProvider="fetchData")
	public void mergeLead(String UName1,String UPwd1) {
		new LoginPage() 
		.Typeusername(UName1) //Note: Semicolon is not kept at the end of line as process to be followed in same page
		.TypePassword(UPwd1)
		.ClickLogin()
		.clickCRMSFA()
		.Clicklead()
		.clickMergeLeadsbtn();
		
		
		
		
		//.ClickLogout();
		
		
		
		
		//.ClickLogin()//do not close parameter as mouse should point to same page
		
		//.ClickLogout();
		
	}
	
	

}





