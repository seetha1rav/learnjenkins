package week6.Day2;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;


public class CreateLeadReadTable extends ProjectMethods{

	@BeforeTest
	public void setData()
	{
		testCaseName="Create Lead";
		testDesc="Create a lead";
		author="Seetha";
		category="smoke";
		dataSheetName="TC001";

	}	

	@Test(dataProvider = "qa")
	public void createLead(String cName, String fName, String lName, String eMail, String PhNo) {
		click(locateElement("LinkText", "Leads"));
		click(locateElement("LinkText", "Create Lead"));

		type(locateElement("id", "createLeadForm_companyName"), cName);
		type(locateElement("id", "createLeadForm_firstName"),fName);
		type(locateElement("id", "createLeadForm_lastName"),lName);
		type(locateElement("id", "createLeadForm_primaryEmail"),eMail);
		type(locateElement("id", "createLeadForm_primaryPhoneNumber"),""+PhNo);
		click(locateElement("name", "submitButton"));		
	}


	//, indices= {3}
	@DataProvider(name ="qa")//Indices of 0 is the first record
	//@DataProvider(name ="qa")
	public Object[][] fetchData() throws IOException
	{
		//return ReadExcelIntegr.read();
		return ReadExcelIntegr.read(dataSheetName);
	}

		/*Object[][] data = new Object[2][5];
		data[0][0] = "TestLeaf";
		data[0][1] = "sarath";
		data[0][2] = "M";
		data[0][3] = "sarath@Testleaf.com";
		data[0][4] = 124567890;

		data[1][0] = "IBM";
		data[1][1] = "karthi";
		data[1][2] = "G";
		data[1][3] = "karthi@IBM.com";
		data[1][4] = 124567891;*/
	


}


