package week6.Day2;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
//import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class FindLeadReadTable extends ProjectMethods{
	//to create FindLead using parameterisation
	
	@BeforeTest
	public void setData()
	{
		testCaseName="Find Lead";
		testDesc="Finding of a lead";
		author="Seetha";
		category="smoke";

	}	

	@Test(dataProvider = "qa")
		// TODO Auto-generated method stub
	public void findleads(String fName1, String lName1, String cName1) throws InterruptedException //Note that the data gets passed in the order of arguments

  {
	 // login();
		WebElement lead = locateElement("LinkText", "Leads");
		click(lead);

	  WebElement Findleads = locateElement("LinkText","Find Leads");
	  click(Findleads);
	  
	  type(locateElement("xpath","(//div[@class='x-form-element'])[19]/input"),fName1);
	  type(locateElement("xpath","(//div[@class='x-form-element'])[20]/input"),lName1);
	  type(locateElement("xpath","(//div[@class='x-form-element'])[21]/input"),cName1);
	  
	  WebElement FindLeadsBtn = locateElement("xpath","//button[text()='Find Leads']");
	  click(FindLeadsBtn);
	  	
		}
	
	  @DataProvider(name="qa", indices= {0})
	  public Object[][] fetchdata() throws IOException
	  {
		  return ReadExcelIntegr.read(dataSheetName);
	  }
	
  }
		
		





