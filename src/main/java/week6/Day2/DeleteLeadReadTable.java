package week6.Day2;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class DeleteLeadReadTable extends ProjectMethods{
	
	
	@BeforeTest
	public void setData()
	{
		testCaseName="Delete Lead";
		testDesc="Delete of a lead";
		author="Seetha";
		category="smoke";

	}	

		@Test(dataProvider = "qa")
		//(dependsOnMethods= {"testcases.TC002CreateLead.CreaLd"})
		public void editLead(String fName, String lName, String cName) throws InterruptedException {
			//To click the Leads tab
			
					
			WebElement lead = locateElement("LinkText", "Leads");
			click(lead);
			
			WebElement Findleads = locateElement("LinkText","Find Leads");
			  click(Findleads);
			  
			  type(locateElement("xpath","(//div[@class='x-form-element'])[19]/input"),fName);
			  type(locateElement("xpath","(//div[@class='x-form-element'])[20]/input"),lName);
			  type(locateElement("xpath","(//div[@class='x-form-element'])[21]/input"),cName);
			  
			  WebElement FindLeadsBtn = locateElement("xpath","//button[text()='Find Leads']");
			  click(FindLeadsBtn);
			
			Thread.sleep(5000);
	    	 click(locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a"));
			
	    	//to click the edit button
	    	 click(locateElement("LinkText", "Delete"));
	    	 
	    	/*
			
			
			WebElement editLeadScreen = locateElement("xpath", "//div[text()='Edit Lead']");
			verifyExactText(editLeadScreen, "Edit Lead");
			
			//To update the value in the Description			
			WebElement descText = locateElement("updateLeadForm_description");
			type(descText, "This is to edit already created lead");
			
			//to click update button
			WebElement updateLead = locateElement("xpath", "//input[@value='Update']");
			click(updateLead);
			*/
			}
		
		
		 @DataProvider(name="qa", indices= {6})
		  public Object[][] fetchdata() throws IOException
		  {
			  return ReadExcelIntegr.read(dataSheetName);
		  }
	} 