package week6.Day2;

import java.io.IOException;

import org.apache.poi.util.SystemOutLogger;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcelIntegr {
//created the static method to be accessible outside and return type as object
	public static Object[][] read(String dataSheetName) throws IOException {
		// TODO Auto-generated method stub
		XSSFWorkbook wb=new XSSFWorkbook("./data/"+dataSheetName+".xlsx");
	//	XSSFWorkbook wb=new XSSFWorkbook("./data/createLead.xlsx");
		
		
		
		XSSFSheet sheet = wb.getSheetAt(0);
		int rowcount = sheet.getLastRowNum();
		
		System.out.println(rowcount);
		int cellcount = sheet.getRow(0).getLastCellNum();
		System.out.println(cellcount);
	
		//added the object
   Object[][] data=new Object[rowcount][cellcount];
		
		for (int j=1;j<=rowcount;j++)
		{
			XSSFRow row = sheet.getRow(j);
			for (int i=0;i<cellcount;i++)
			{
				
				XSSFCell cell = row.getCell(i);
				try {
					String value = cell.getStringCellValue();
					System.out.println(value);
					data[j-1][i]=value;
				} catch (NullPointerException e) {
					// TODO Auto-generated catch block
					System.out.println("");
				}
			}
		}
		wb.close();
		return data;
		
		

	}

}
