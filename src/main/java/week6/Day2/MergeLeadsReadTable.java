package week6.Day2;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class MergeLeadsReadTable extends ProjectMethods{

	@BeforeTest
	public void setData()
	{
		testCaseName="merge Lead";
		testDesc="Merge leads";
		author="Seetha";
		category="regression";

	}//second item in excel has 2 records - hence indices =1
	
@Test(dataProvider = "qa")
	public void mergeLead(String fName, String lName, String cName) throws InterruptedException  {
		
						
		//click Leads tab
		WebElement leads = locateElement("xpath", "//a[text()='Leads']");
		click(leads);
		
		//Click Merge Leads
		WebElement mergeLead = locateElement("LinkText", "Merge Leads");
		click(mergeLead);
		
		//select Find icon in from lead
				WebElement fromIcon = locateElement("xpath", "//table[@name='ComboBox_partyIdFrom']/following-sibling::a/img");
				click(fromIcon);
				

				switchToWindow(1);
				driver.manage().window().maximize();
				
				
				WebElement fNameTo = locateElement("xpath", "//input[@name='firstName']");
				type(fNameTo, "Archana");
				/*type(locateElement("name","firstName"),fName);
				type(locateElement("name","lastName"),lName);
				type(locateElement("name","companyName"),cName);*/
				
						
				WebElement findLeadButton = locateElement("xpath","//button[text()='Find Leads']");
				click(findLeadButton);
				Thread.sleep(3000);
				
				WebElement selectResult = locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[1]");
				clickWithoutSnap(selectResult);//That window closes after fetching. hence provide clickwithout snap

			
				switchToWindow(0);


				//select Find icon in To lead
				WebElement toIcon = locateElement("xpath", "//table[@name='ComboBox_partyIdTo']/following-sibling::a/img");
				click(toIcon);
				Thread.sleep(3000);
				
				switchToWindow(1);
				driver.manage().window().maximize();
				
				
				type(locateElement("name","firstName"),fName);
				type(locateElement("name","lastName"),lName);
				type(locateElement("name","companyName"),cName);
				
				//type(locateElement("name","firstName"),fName);
				
				//type(locateElement("name","lastName"),lName);
			//	type(locateElement("name","companyName"),cName);
										
				WebElement findLeadButtonTo = locateElement("xpath", "//button[text()='Find Leads']");
				click(findLeadButtonTo);
				
				Thread.sleep(3000); //Without thread.sleep, the next element is not identified
				
				WebElement selectResultTo = locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[1]");
				clickWithoutSnap(selectResultTo);
				
				switchToWindow(0);
				
				WebElement mergeButton = locateElement("xpath","//a[text()='Merge']");
				clickWithoutSnap(mergeButton);
				
				
			//handle alert
				getAlertText();
				acceptAlert();
				//getAlertText();
				
				
				//Click Find Leads link
				WebElement findLeads = locateElement("LinkText", "Find Leads");
				click(findLeads);
				
				WebElement findLeadButton1 = locateElement("xpath", "//button[text()='Find Leads']");
				click(findLeadButton1);
			
	}

@DataProvider(name="qa", indices= {1})
public Object[][] fetchdata() throws IOException
{
	return ReadExcelIntegr.read(dataSheetName);
}


}