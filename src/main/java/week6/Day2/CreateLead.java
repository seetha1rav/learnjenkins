package week6.Day2;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import pages.HomePage;
import pages.LoginPage;
import pages.MyHome;
import wdMethods.ProjectMethods;

public class CreateLead extends ProjectMethods{
	public CreateLead()
	{
	PageFactory.initElements(driver, this);	
	}
	
	

	
	/*public CreateLead   TypeFirstName(String Fname) {
		type(eleFName,Fname);
		return this;
	}*/

	
	
	@BeforeTest
	public void setData()
	{
		testCaseName="Create Lead";
		testDesc="Create a lead";
		author="Seetha";
		category="smoke";
		dataSheetName="TC001";

	}	

	//@DataProvider(name="fetchData", indices= {1})
	@Test(dataProvider="fetchData")
	public void createLead(String Uname,String pwd) {
		new LoginPage()
		.Typeusername(Uname)
		.TypePassword(pwd)
		.ClickLogin()
		.clickCRMSFA();
		
		
		
		
		
		//.ClickLogin()//do not close parameter as mouse should point to same page
		
		//.ClickLogout();
		
	}

}
