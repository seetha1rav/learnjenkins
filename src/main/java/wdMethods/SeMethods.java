package wdMethods;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

import com.aventstack.extentreports.Status;

import utils.Report;

public class SeMethods extends Report implements  WdMethods{
	public int i = 1;
	//Driver object should be static
	public static RemoteWebDriver driver;

	public void startApp(String browser, String url) {   
		try {
			if (browser.equalsIgnoreCase("chrome")) {			
				System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
				//To disable facebook popup in browser
				ChromeOptions options = new ChromeOptions();
				options.addArguments("--disable-notifications");
							
				//pass the object to the chromedriver constructor
				driver = new ChromeDriver(options);
			} else if (browser.equalsIgnoreCase("firefox")) {			
				System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
				driver = new FirefoxDriver();
			}
			driver.get(url);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			reportStep("Pass","The Browser "+browser+" is Launched Successfully " );
		} catch (WebDriverException e) {
			reportStep("Fail", "The Browser "+browser+" not Launched ");
		} finally {
			takeSnap();			
		}
	}


	public WebElement locateElement(String locator, String locValue) {
		try {
			switch(locator) {
			case "id": return driver.findElementById(locValue);
			case "class": return driver.findElementByClassName(locValue);
			case "xpath": return driver.findElementByXPath(locValue);
			case "LinkText": return driver.findElementByLinkText(locValue);
			case "name": return driver.findElementByName(locValue);
			} reportStep("Passed", "Report executed successfully");
		} catch (NoSuchElementException e) {
		    reportStep("Fail", "The Element Is Not Located ");
		}
		return null;
	}

	@Override
	public WebElement locateElement(String locValue) {
		return driver.findElementById(locValue);
	}

	@Override
	public void type(WebElement ele, String data) {
		try {
			ele.sendKeys(data);
			reportStep("PASS", "The data"+data+"is typed successfully");
		} catch (WebDriverException e) {
			reportStep("FAIL", "The data"+data+"is not typed");
			
		}
	}

	@Override
	public void click(WebElement ele) {
		try {
			ele.click();
			reportStep("Pass", "The Element "+ele+" Clicked Successfully");
		} catch (WebDriverException e) {
			reportStep("Fail", "The Element "+ele+" not Clicked");
		}
		
	}
	
	public void clearText(WebElement ele) {
		try {
			ele.clear();
			reportStep("Pass", "Text cleared");
		} catch (WebDriverException e) {
		reportStep("Fail", "Text not cleared");
		}
		
	}
	
	public void clickWithoutSnap(WebElement ele) {
		ele.click();
		System.out.println("The Element "+ele+" Clicked Successfully Without Taking Snap");	
	}

	@Override
	public String getText(WebElement ele) {
		String text = ele.getText();
		return text;
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		try {
			Select dd = new Select(ele);
			dd.selectByVisibleText(value);
			reportStep("Pass", "DropDown is selected with Value"+value);
		} catch (WebDriverException e) {
			reportStep("Fail", "DropDown By Value is not Selected");
		}
		
	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		try {
			Select dd = new Select(ele);
			dd.selectByIndex(index);
			reportStep("Pass", "The DropDown Is Selected with "+index);
		} catch (WebDriverException e) {
			reportStep("Fail", "The DropDown Is Not Selected with "+index);
		}
		

	}

	@Override
	public boolean verifyTitle(String expectedTitle) {
		if(driver.getTitle().equals(expectedTitle)){
			
		}
		return false;
	}

	@Override
	public void verifyExactText(WebElement ele, String expectedText) {
		
		if(ele.getText().equals(expectedText)) {
			System.out.println("Text Verified: True");
		}else {System.out.println("Text Verified: False");}

	}

	@Override
	public void verifyPartialText(WebElement ele, String expectedText) {
		if(ele.getText().contains(expectedText))
		{
			reportStep("Pass", "Text Verified");
		}
		else
			reportStep("Fail", "Text not matches");
		/*{
			System.out.println("Text Verified: True");
		}else 
		{
			System.out.println("Text Verified: False");
		}*/

	}

	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) {
		if(ele.getAttribute(value).equals(value)){
			System.out.println("Attribute Value Verified: True");
		}else {System.out.println("Attribute Value Verified: False");
		}
		

	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
	        if(ele.getAttribute(value).contains(value)) {
	        	System.out.println("Attribute Value Verified: True");
			}else {System.out.println("Attribute Value Verified: False");
			}
	        }

	

	@Override
	public void verifySelected(WebElement ele) {
		if(ele.isSelected()) {
			System.out.println(ele+" is Selected");
		}else {System.out.println(ele+" is not Selected");
		ele.click();
		}
		}

	

	@Override
	public void verifyDisplayed(WebElement ele) {
		if(ele.isDisplayed()) {
			System.out.println(ele+" is Displayed");
		}else {System.out.println(ele+" is not Selected");
		}
		}

	

	@Override
	public void switchToWindow(int index) {
		Set<String> allWindows = driver.getWindowHandles();
		List<String> listOfWindow = new ArrayList<String>();
		listOfWindow.addAll(allWindows);
		driver.switchTo().window(listOfWindow.get(index));
		System.out.println("The Window is Switched ");
	}

	@Override
	public void switchToFrame(WebElement ele) {
		driver.switchTo().frame(ele);
		System.out.println("The Frame is Switched ");
	}

	@Override
	public void acceptAlert() {
		driver.switchTo().alert().accept();
		
	}

	@Override
	public void dismissAlert() {
		driver.switchTo().alert().dismiss();

	}

	@Override
	public String getAlertText() {
		driver.switchTo().alert().getText();
		return null;
	}
	
	public void SendAlertText(String text) {
		driver.switchTo().alert().sendKeys(text);
	}

	@Override
	public void takeSnap() {
		try {
		File src = driver.getScreenshotAs(OutputType.FILE);
		File dsc = new File("./snaps/img"+i+".png");
			FileUtils.copyFile(src, dsc);
		} catch (IOException e) {
			e.printStackTrace();
		}
		i++;
	}
	
	public void noSnap() {
		
		
	}
	
	

	@Override
	public void closeBrowser() {
		driver.close();

	}

	@Override
	public void closeAllBrowsers() {
		driver.quit();

	}

}
