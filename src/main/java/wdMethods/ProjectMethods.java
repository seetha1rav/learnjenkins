package wdMethods;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import week6.Day2.ReadExcelIntegr;

public class ProjectMethods extends SeMethods {
	//necessary to refer the excel sheet
	public String dataSheetName;
	
	@BeforeSuite
	public void beforeSuite() {
		startResult();
	}
	
	@Parameters({"url"})
	@BeforeMethod
	//(groups= {"any"})
	public void login(String url) {
		
		beforeMethod();
		startApp("chrome", url);
		/*WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, username);
		WebElement elePassword = locateElement("password");
		type(elePassword, password);
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
		WebElement eleCrm = locateElement("LinkText", "CRM/SFA");
		click(eleCrm);
		*/
		
		//https://www.zoomcar.com/chennai
		
		//https://www.facebook.com/

	}
	
	
		
	


	//@AfterMethod
	public void closeApp() {
		closeBrowser();
	}
	
	@AfterSuite
	public void endTestCase() {
		endResult();
	}
	
	//as the value is passed for all Testcases, DataProvider is provided here
	@DataProvider(name="fetchData", indices= {0})
	public Object[][] getData() throws IOException{
{
	ReadExcelIntegr re= new ReadExcelIntegr();
	Object[][] sheet =re.read(dataSheetName);
	return sheet;
	
}
}}
