package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MyHome extends ProjectMethods{
	public MyHome()
	{
	PageFactory.initElements(driver, this);	
	}
	
	
	//To create webElement for CreateLeadslink
	@FindBy(how=How.LINK_TEXT, using ="Create Lead") WebElement CreateLeadLink;

	public CreateLead clickCreateLead() {
		click(CreateLeadLink);
		return new CreateLead() ;
	}

	@FindBy(how=How.LINK_TEXT, using="Leads") WebElement eleLeads;
	
	
	public MyLeads Clicklead() {
		click(eleLeads);
		return new MyLeads();
	}
	
	
	
	
	
	
	
	
	

	
	
	
	
	}