package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import pages.HomePage;
import pages.LoginPage;
import pages.MyHome;
import wdMethods.ProjectMethods;

public class CreateLead extends ProjectMethods{
	public CreateLead()
	{
	PageFactory.initElements(driver, this);	
	}
	
	//to create locateElements for FirstName, LastName and CompanyName
	@FindBy(how=How.ID,using="createLeadForm_firstName") WebElement eleFname;
	@FindBy(how=How.ID,using="createLeadForm_lastName") WebElement eleLName;
	@FindBy(how=How.ID,using="createLeadForm_companyName") WebElement eleCName;
	@FindBy(how=How.NAME, using="submitButton") WebElement eleCreateLeadSubmit;
	
	
	
		
	public CreateLead   TypeFirstName(String FName) {
		type(eleFname,FName);
		return this;
	}
	
	public CreateLead   TypeLastName(String LName) {
		type(eleLName,LName);
		return this;
	}
	
	public CreateLead   TypeCompanyName(String CName) {
		type(eleCName,CName);
		return this;
	}

	public LiewLead clickCreateLeadbtn()
	{
		click(eleCreateLeadSubmit);
		return new LiewLead();
	}
	
	
	
	
	
	
	
	

	/*//@DataProvider(name="fetchData", indices= {1})// note:indices cannot be passed here as it is explained in ProjectMethods
	@Test(dataProvider="fetchData")
	public void createLead(String UName1,String UPwd1, String FName1,String LName1, String CName1) {
		new LoginPage() 
		.Typeusername(UName1) //Note: Semicolon is not kept at the end of line as process to be followed in same page
		.TypePassword(UPwd1)
		.ClickLogin()
		.clickCRMSFA()
		.clickCreateLead()
		.TypeFirstName(FName1)
		.TypeLastName(LName1)
		.TypeCompanyName(CName1)
		.clickCreateLeadbtn();*/
		
		
		
		//.ClickLogout();
		
		
		
		
		//.ClickLogin()//do not close parameter as mouse should point to same page
		
		//.ClickLogout();
		
	}


