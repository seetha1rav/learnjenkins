package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import pages.HomePage;
import pages.LoginPage;
import pages.MyHome;
import wdMethods.ProjectMethods;

public class MergeLeads extends ProjectMethods{
	public MergeLeads()
	{
	PageFactory.initElements(driver, this);	
	}
	
	
	@FindBy(how=How.XPATH, using="//table[@id='widget_ComboBox_partyIdFrom']/following::img") WebElement eleFromLead;
	@FindBy(how=How.XPATH, using="//table[@id='widget_ComboBox_partyIdTo']/following::img") WebElement eleToLead;
	@FindBy(how=How.XPATH, using="//a[text()='Merge']") WebElement eleMerge;
	
	public MyLeads ClickFromLead() {
		click(eleFromLead);
		return new MyLeads();
	}
	
	
	
/*	@BeforeTest
	public void setData()
	{
		testCaseName="Merge Lead";
		testDesc="Merge a lead";
		author="Seetha";
		category="smoke";
		dataSheetName="MergeLead";//name of the excel file in the path

	}	

	//@DataProvider(name="fetchData", indices= {1})// note:indices cannot be passed here as it is explained in ProjectMethods
	@Test(dataProvider="fetchData")
	public void mergeLead(String UName1,String UPwd1) {
		new LoginPage() 
		.Typeusername(UName1) //Note: Semicolon is not kept at the end of line as process to be followed in same page
		.TypePassword(UPwd1)
		.ClickLogin()
		.clickCRMSFA()
		.Clicklead()
		.clickMergeLeadsbtn();
		
		
		
		
		//.ClickLogout();
		
		
		
		
		//.ClickLogin()//do not close parameter as mouse should point to same page
		
		//.ClickLogout();
		
	}*/

}
