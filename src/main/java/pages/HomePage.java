package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class HomePage extends ProjectMethods{
	public HomePage()
	{
	PageFactory.initElements(driver, this);	
	}
	
	@FindBy(how=How.PARTIAL_LINK_TEXT, using="CRM/SFA") WebElement clickCRMSFA;
	
	//@FindBy(how=How.ID, using ="createLeadForm_firstName") WebElement eleFName;
	
	
	public MyHome   clickCRMSFA() {
		click(clickCRMSFA);
		return new MyHome();

	}


@FindBy(how=How.CLASS_NAME, using ="decorativeSubmit") WebElement eleLogout;

	
		
	public LoginPage   ClickLogout() {
		click(eleLogout);
		return new LoginPage();
		
		
		

	}
	
	
	
	
	}