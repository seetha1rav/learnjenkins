package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import pages.HomePage;
import pages.LoginPage;
import pages.MyHome;
import wdMethods.ProjectMethods;

public class MyLeads extends ProjectMethods{
	public MyLeads()
	{
	PageFactory.initElements(driver, this);	
	}
	

	
	@BeforeTest
	public void setData()
	{
		testCaseName="My Lead";
		testDesc="My lead";
		author="Seetha";
		category="smoke";
		dataSheetName="MergeLead";//name of the excel file in the path

	}	
	
	//to create webElement for MergeLeads link
			@FindBy(how=How.LINK_TEXT, using="Merge Leads") WebElement eleMergeLeads;
			
				
				public MergeLeads clickMergeLeadsbtn()
			{
				click(eleMergeLeads);
				return new MergeLeads();
			}


	//@DataProvider(name="fetchData", indices= {1})// note:indices cannot be passed here as it is explained in ProjectMethods
	/*@Test(dataProvider="fetchData")
	public void View(String UName1,String UPwd1) {
		new LoginPage() 
		.Typeusername(UName1) //Note: Semicolon is not kept at the end of line as process to be followed in same page
		.TypePassword(UPwd1)
		.ClickLogin()
		.clickCRMSFA()
		
		
		
		
		//.ClickLogout();
		
		
		
		
		//.ClickLogin()//do not close parameter as mouse should point to same page
		
		//.ClickLogout();
		
	}*/

}
